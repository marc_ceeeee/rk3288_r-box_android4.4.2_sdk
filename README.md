# How to Download #
The SDK is huge (about 4 GiB). Please **DO NOT CLONE** from Bitbucket directly (It will fail due to network flow limit).

We have provided a tar ball of the .git directory at:

* [Baidu Pan](http://pan.baidu.com/s/1dDhDNGX#path=%252FDevBoard%252FFirefly-RK3288%252FSource%252FAndroid4.4.2)
* [Google Drive](https://drive.google.com/folderview?id=0B7HO8lbGgAqAdU1tbnNkY25udFE&usp=sharing)

Please check the md5 checksum before proceeding:
```
#!shell
$ md5sum /path/to/firefly-rk3288_sdk_git.tar.gz
0db2e0c5c2a1cb344f923cd538ed9443  firefly-rk3288_sdk_git.tar.
```

If it is correct, uncompress it:
```
#!shell
mkdir -p ~/proj/firefly-rk3288
cd ~/proj/firefly-rk3288
tar xf /path/to/firefly-rk3288_sdk_git.tar.gz
git reset --hard
git remote add bitbucket https://TeeFirefly@bitbucket.org/T-Firefly/firefly-rk3288.git
```

From now on, you can pull updates from Bitbucket:
```
#!shell
git pull bitbucket master:master
```

Please visit the following website for more details of our Firefly-RK3288 devolopment board. Thank you.

http://www.t-firefly.com/en/

# 如何下载 #
SDK 非常巨大（约 4GiB），请**不要直接从 Bitbucket 下载 **仓库源码（由于流量限制的原因会出错）。

我们提供了 .git 目录的打包，放到云盘上以供下载：

* [百度云盘](http://pan.baidu.com/s/1dDhDNGX#path=%252FDevBoard%252FFirefly-RK3288%252FSource%252FAndroid4.4.2)
* [Google Drive](https://drive.google.com/folderview?id=0B7HO8lbGgAqAdU1tbnNkY25udFE&usp=sharing)

下载完成后先验证一下 MD5 码：
```
#!shell
$ md5sum /path/to/firefly-rk3288_sdk_git.tar.gz
0db2e0c5c2a1cb344f923cd538ed9443  firefly-rk3288_sdk_git.tar.gz
```

确认无误后，就可以解压：
```
#!shell
mkdir -p ~/proj/firefly-rk3288
cd ~/proj/firefly-rk3288
tar xf /path/to/firefly-rk3288_sdk_git.tar.gz
git reset --hard
git remote add bitbucket https://TeeFirefly@bitbucket.org/T-Firefly/firefly-rk3288.git
```

之后就可以从 Bitbucket 处获取更新的提交，保持同步:
```
#!shell
git pull bitbucket master:master
```

想了解更多我们 Firefly-RK3288 开发板的详情，请浏览以下网址，谢谢！

  http://www.t-firefly.com